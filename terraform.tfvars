app_name = "ebtf"
app_zone = "ebtf.aws.tomdavidson.org"
tags = {
  env = "exp"
  owner = "tomd2"
}
description = "An experimental app exploring dynamic Elastic BeanStalk Environments with Terraform."
