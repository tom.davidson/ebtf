// This is a generated file form the 'apps-ops' module and shares its
// lifecycle. Rather than customize this file it is better to override it.
//
// Usage:
// terraform env new app-ops
// terraform get iac/app-ops
// terraform plan iac/app-ops
// terraform apply iac/app-ops
// terraform init -force-copy -backend-config=app-ops-backend.tfvars

acl = "private"
bucket = "${bucket}"
dynamodb_table = "${lock_table}"
encrypt = true
key = "${key}"