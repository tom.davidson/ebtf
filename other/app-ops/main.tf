variable "required_tags" {
  default = {
    data-sensitivity = "confidential"
    env              = "production"
  }

  description = <<EOF
(internal)
Reasouce tags for the created reasouces. Set 'tags' rather than overide this to
add more tags.
EOF
}

resource "aws_s3_bucket" "ops" {
  bucket = "${var.name_prefix}${var.name}${var.name_suffix}-ops"
  acl    = "private"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  force_destroy = true
  tags          = "${merge(var.tags, var.required_tags)}"
}

resource "aws_dynamodb_table" "ops" {
  name           = "${var.name_prefix}${var.name}${var.name_suffix}-ops"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = "${merge(var.tags, var.required_tags)}"
}

# resource "aws_sns_topic" "ops" {
#   name = "${var.name_prefix}${var.name}${var.name_suffix}-ops"

#   lifecycle {
#     prevent_destroy = true
#   }
# }

data "template_file" "backend_vars" {
  template = "${file("${path.module}/acs-backend.tfvars.tpl")}"

  vars {
    bucket     = "${aws_s3_bucket.ops.id}"
    key        = "state/terraform.tfstate"
    lock_table = "${aws_dynamodb_table.ops.id}"
  }
}

resource "local_file" "backend_vars" {
  content  = "${data.template_file.backend_vars.rendered}"
  filename = "app-ops-backend.tfvars"
}

data "template_file" "app_ops_backend" {
  template = "${file("${path.module}/app-ops.tf.tpl")}"
}

resource "local_file" "app_ops_backend" {
  content  = "${data.template_file.app_ops_backend.rendered}"
  filename = "app-ops-backend.tf"
}
