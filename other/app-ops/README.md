# AppOps

Common operational resources for your app using ACS:
  1. SNS Topic
  1. DynomoDB Table for TF State Locking
  1. S3 Bucket for TF State, logs, and artifacts
  1. Generated TF plan files

> The remote state is initialized outside of the plan due to `terraform.backend: configuration cannot contain interpolations` [#13022](https://github.com/hashicorp/terraform/issues/13022)

## usage

add module to plan your plan, perhaps a separate plan:

```hcl
module "app_ops" {
  source = "./app-ops"
  name   = "${var.name}"
  tags   = "${var.tags}"
}
```

Apply the module, only the module:

```sh
$ terraform apply -target module.app_ops
```

Inspect the generated files that have been added to your plan:

  1. `app-ops-backend.tf`
  1. `app-ops-backend.tfvar`

Initialize the remote state using generated config:

```sh
$ terraform init -force-copy -backend-config app-ops-backend.tfvar
```

Ready Player One...