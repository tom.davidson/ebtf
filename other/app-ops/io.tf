variable "name" {
  description = "Name of the named resources"
}

variable "name_prefix" {
  default = ""

  description = "Name prefix of named resources."
}

variable "name_suffix" {
  default = ""

  description = "Name suffix of named resources."
}

variable "tags" {
  default = {}

  description = <<EOF
(Optional)
Reasouce tags for the created reasouces. 
EOF
}

output "state_file" {
  value       = "${terraform.env}/state/terraform.tfstate"
  description = "The S3 key name of the configured statefile."
}

output "bucket_id" {
  value       = "${aws_s3_bucket.ops.id}"
  description = "The name of the S3 bucket to store state and other AppOps Files."
}

output "bucket_arn" {
  value       = "${aws_s3_bucket.ops.arn}"
  description = "The ARN of the S3 bucket to store state and other AppOps Files."
}

output "lock_table_id" {
  value       = "${aws_dynamodb_table.ops.id}"
  description = "The name of a DynamoDB table to use for state locking."
}

output "lock_table_arn" {
  value       = "${aws_dynamodb_table.ops.arn}"
  description = "The ARN of a DynamoDB table to use for state locking."
}

# output "sns_topic_arn" {
#   value       = "${aws_sns_topic.ops.arn}"
#   description = "The ARN of the AppOps SNS topic."
# }

