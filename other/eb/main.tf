resource "aws_elastic_beanstalk_application" "default" {
  name        = "${var.app_name}"
  description = "${var.description}"
}

resource "aws_route53_zone" "app" {
  name          = "${var.app_zone}"
  comment       = "${var.description}"
  tags          = "${var.tags}"
  force_destroy = true
}

resource "aws_route53_record" "app_ns" {
  zone_id = "${aws_route53_zone.app.zone_id}"
  name    = "${var.app_zone}"
  type    = "NS"
  ttl     = "30"
  records = ["${aws_route53_zone.app.name_servers}"]
}

data "aws_route53_zone" "parent" {
  name = "${replace(var.app_zone, "/^[^.]*\\./", "")}"
}

resource "aws_route53_record" "parent_ns" {
  zone_id = "${data.aws_route53_zone.parent.zone_id}"
  name    = "${var.app_zone}"
  type    = "NS"
  ttl     = "30"
  records = ["${aws_route53_zone.app.name_servers}"]
}
