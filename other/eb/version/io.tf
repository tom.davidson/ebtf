variable "version_name" {
  type = "string"

  description = "A unique name for this version."
}

variable "description" {
  default = ""

  description = <<EOF
(Optional)
Short description of the Environment
EOF
}

variable "tags" {
  default = {}

  description = <<EOF
(Optional)
A set of tags to apply to the Environment's taggable reasouces. 
EOF
}

variable "app_name" {
  type = "string"

  description = "The EB Applicaiton to provision this version."
}

variable "bucket" {
  type = "string"

  description = "The S3 bucket used to store version source bundles."
}

variable "docker_port" {
  default = 8000

  description = <<EOF
(Optional)
The docker port that EB Env's proxy should forward to.
EOF
}

variable "docker_image" {
  type = "string"

  description = <<EOF
Full URL for repo, image name, and tag that the EB Version will pull.
EOF
}

output "verion_name" {
  value = "${aws_elastic_beanstalk_application_version.default.name}"
}
