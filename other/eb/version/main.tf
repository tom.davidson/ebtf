resource "aws_elastic_beanstalk_application_version" "default" {
  application = "${var.app_name}"
  bucket      = "${var.bucket}"
  count       = "${var.version_name != "" ? 1 : 0}"
  description = "${var.description}"
  key         = "${aws_s3_bucket_object.version.id}"
  name        = "${var.version_name}"
}

resource "aws_s3_bucket_object" "version" {
  count  = "${var.version_name != "" ? 1 : 0}"
  bucket = "${var.bucket}"
  etag   = "${data.archive_file.source_bundle.output_sha}"
  key    = "versions/${var.version_name}.zip"
  source = "${path.module}/source-bundle.zip"
}

data "archive_file" "source_bundle" {
  count       = "${var.version_name != "" ? 1 : 0}"
  output_path = "${path.module}/source-bundle.zip"
  type        = "zip"

  source {
    content  = "${data.template_file.dockerrun.rendered}"
    filename = "Dockerrun.aws.json"
  }
}

data "template_file" "dockerrun" {
  count    = "${var.version_name != "" ? 1 : 0}"
  template = "${file("${path.module}/Dockerrun.aws.json")}"

  vars {
    port  = "${var.docker_port}"
    image = "${var.docker_image}"
  }
}
