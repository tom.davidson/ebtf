resource "aws_elastic_beanstalk_environment" "default" {
  application            = "${var.app_name}"
  cname_prefix           = "${var.cname_prefix}"
  count                  = "${var.template_name == "" ? 1 : 0}"
  name                   = "${var.env_name != "" ? var.env_name : terraform.env }"
  solution_stack_name    = "${data.aws_elastic_beanstalk_solution_stack.default.name}"
  description            = "${var.description}"
  tags                   = "${var.tags}"
  tier                   = "${var.tier}"
  version_label          = "${var.version_name}"
  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"
}

resource "aws_elastic_beanstalk_environment" "template" {
  application            = "${var.app_name}"
  cname_prefix           = "${var.cname_prefix}"
  count                  = "${var.template_name != "" ? 1 : 0}"
  name                   = "${var.env_name != "" ? var.env_name : terraform.env }"
  tags                   = "${var.tags}"
  template_name          = "${var.template_name}"
  tier                   = "${var.tier}"
  version_label          = "${var.version_name}"
  wait_for_ready_timeout = "${var.wait_for_ready_timeout}"
}

# the second coalesce param should be  aws_elastic_beanstalk_environment.template.name,
# but terraform is throwing an error.
resource "aws_route53_record" "default" {
  name = "${var.env_zone_name != "" ? var.env_zone_name : 
          coalesce(
            aws_elastic_beanstalk_environment.default.name,
            ""
           )}.${var.app_zone_name}"

  type    = "A"
  zone_id = "${var.app_zone_id}"

  alias {
    name = "${aws_elastic_beanstalk_environment.default.cname}"

    zone_id = "${lookup(
      var.AWS_Elastic_Beanstalk_Zone_IDs,
      element(
        split(".", aws_elastic_beanstalk_environment.default.cname), 
        length(split(".", aws_elastic_beanstalk_environment.default.cname))-3
      ),
      "zone-id-not-found"
    )}"

    evaluate_target_health = true
  }
}

data "aws_elastic_beanstalk_solution_stack" "default" {
  most_recent = true
  name_regex  = "^64bit Amazon Linux (.*) ${lookup(var.stack_pat, var.stack)} (.*)$"
}

variable "stack_pat" {
  default = {
    docker         = "running Docker"
    docker-multi   = "running Multi-container Docker"
    go             = "running Go"
    java           = "running Java 8"
    node           = "running Node.js"
    packer         = "running Packer"
    php            = "running PHP 7.0"
    python         = "running Python 3.4"
    ruby-passenger = "running Ruby 2.3 (Passenger Standalone)"
    ruby-puma      = "running Ruby 2.3 (Puma)"
    tomcat         = "running Tomcat 8 Java 8"
  }

  description = <<EOF
(internal)
Simple look up table to assist in precise regex patterns that will match on only
one solution stack while keeping the input succinct.
EOF
}

variable "AWS_Elastic_Beanstalk_Zone_IDs" {
  default = {
    ap-northeast-1 = "Z1R25G3KIG2GBW"
    ap-northeast-2 = "Z3JE5OI70TWKCP"
    ap-south-1     = "Z18NTBI3Y7N9TZ"
    ap-southeast-1 = "Z16FZ9L249IFLT"
    ap-southeast-2 = "Z2PCDNR3VC2G1N"
    ca-central-1   = "ZJFCZL7SSZB5I"
    eu-central-1   = "Z1FRNW7UH4DEZJ"
    eu-west-1      = "Z2NYPWQ7DFZAZH"
    eu-west-2      = "Z1GKAAAUGATPF1"
    sa-east-1      = "Z10X7K2B4QSOFV"
    us-east-1      = "Z117KPS5GTRQ2G"
    us-east-2      = "Z14LCN19Q5QHIC"
    us-west-1      = "Z1LQECGX5PH1X"
    us-west-2      = "Z38NKT9BP95V3O"
  }

  description = <<EOF
(internal)
Simple look up table that corrlates AWS EB Zone ID with respective regions.
EOF
}

#

