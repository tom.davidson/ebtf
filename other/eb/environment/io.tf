variable "env_name" {
  default = ""

  description = <<EOF
(optional)
A unique name for this Environment. This name is used in the application URL"
Note: If unset, terraform.env is used.
EOF
}

variable "description" {
  default = ""

  description = <<EOF
(Optional)
Short description of the Environment
EOF
}

variable "tags" {
  default = {}

  description = <<EOF
(Optional)
A set of tags to apply to the Environment's taggable reasouces. 
EOF
}

variable "app_name" {
  type = "string"

  description = <<EOF
(optional)
The Elastic Beanstalk Application specified for this environment.
Note: If unset, configured remote terraform state is used.
EOF
}

variable "cname_prefix" {
  default = ""

  description = <<EOF
(Optional)
Prefix to use for the fully qualified DNS name of the Environment.
EOF
}

variable "version_name" {
  default = ""

  description = <<EOF
The name of the Elastic Beanstalk Application Version to use in deployment.
EOF
}

variable "tier" {
  default = "WebServer"

  description = <<EOF
(Default: WebServer)
Elastic Beanstalk Environment tier. Valid values are Worker or WebServer.
EOF
}

variable "template_name" {
  default = ""

  description = <<EOF
(Optional)
The name of the Elastic Beanstalk Configuration template to use in deployment
EOF
}

variable "stack" {
  default = "docker"

  description = <<EOF
(Defualt: docker)
Looks up the lastest matching solution stack name to use in the EB Environment.
Valided options are (its easy to add others):
  * docker
  * docker-multi
  * go
  * java
  * node
  * packer
  * php
  * python
  * ruby-passenger
  * ruby-puma
  * tomcat
EOF
}

variable "wait_for_ready_timeout" {
  default = "20m"

  description = <<EOF
(Default: 20m)
The maximum duration that Terraform should wait for an Elastic Beanstalk 
Environment to be in a ready state before timing out.
EOF
}

variable "env_zone_name" {
  default = ""

  description = <<EOF
(optional)
The zone or subdomain for the EB Env.
Note: If unset, terraform.env|var.name is used.
EOF
}

variable "app_zone_name" {
  type = "string"

  description = <<EOF
EOF
}

variable "app_zone_id" {
  type = "string"

  description = <<EOF
EOF
}

# output "exists" {
#   value = "${module.eb_envs.exists}"


#   description = "EB Env already exists and will not be created."
# }


# id - ID of the Elastic Beanstalk Environment.
# name - Name of the Elastic Beanstalk Environment.
# description - Description of the Elastic Beanstalk Environment.
# tier - The environment tier specified.
# application – The Elastic Beanstalk Application specified for this environment.
# setting – Settings specifically set for this Environment.
# all_settings – List of all option settings configured in the Environment. These are a combination of default settings and their overrides from setting in the configuration.
# cname - Fully qualified DNS name for the Environment.
# autoscaling_groups - The autoscaling groups used by this environment.
# instances - Instances used by this environment.
# launch_configurations - Launch configurations in use by this environment.
# load_balancers - Elastic load balancers in use by this environment.
# queues - SQS queues in use by this environment.
# triggers - Autoscaling triggers in use by this environment.

