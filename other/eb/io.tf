variable "app_name" {
  type        = "string"
  description = "Name of the named resources"
}

variable "description" {
  default = ""

  description = <<EOF
(Optional)
Short description of the Application.
EOF
}

variable "app_zone" {
  type = "string"

  description = <<EOF
The app's base zone such that app_name.base_zone will be the FQDN.
Note: must already exsit as a delegated zone in R53
Note: app_name.base_zone will be used as the parent zone for EB Environments
EOF
}

variable "tags" {
  default = {}

  description = <<EOF
(Optional)
A set of tags to apply to the Application's taggable reasouces. 
EOF
}

output "zone_id" {
  value = "${aws_route53_zone.app.zone_id}"
}

output "zone_name" {
  value = "${var.app_zone}"
}

output "name" {
  value = "${aws_elastic_beanstalk_application.default.name}"
}
