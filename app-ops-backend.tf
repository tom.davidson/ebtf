// The following is generated from the 'app-ops' module and the file shares the
// module's lifecycle. Rather than customize this file it is better to override 
// it through the module's configuration.
//
// Usage:
// terraform env new app-ops
// terraform get iac/app-ops
// terraform plan iac/app-ops
// terraform apply iac/app-ops
// terraform init -force-copy -backend-config=app-ops-backend.tfvars

variable "bucket" {}
variable "key" {}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "app_ops" {
  backend     = "s3"
  environment = "app-ops"

  config {
    bucket = "${var.bucket}"
    key    = "${var.key}"
  }
}
