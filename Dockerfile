FROM busybox

ARG PORT
ENV PORT=${PORT:-8000}
ARG WHO
ENV WHO=${WHO:-World}
EXPOSE $PORT

RUN echo "while true ; do nc -l -p $PORT < /index.html ; done" >> /run.sh \
    && chmod +x run.sh
COPY src/index.html /index.html.tpl
RUN sed "s/{who}/$WHO/" < /index.html.tpl > /index.html

CMD ["sh", "-c", "/run.sh"]
