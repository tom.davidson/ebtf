# EB, TF, and GitLab PoC

NOTE: the long lived master env is decommissioned to save a few $$

POC to explore GitLab CI dynamic environments, Terraform
Environments and AWS Elastic BeanStalk (EB). While the app is setup for continuous
delivery, there are missing elements blocking production. 

The EB environment deployed from HEAD of the integration branch, master, can be
viewed at [master.ebtf.aws.tomdavidson.org](http://master.ebtf.aws.tomdavidson.org).


## How

### provisioning

I tend to prefer provisioning over CLI clients and configuration management and 
Terraform is the choice provisioner, even in AWS only environments.

I tend to treat Terraform as composable modules for applications beyond my 
current task. The developed modules have code in repo that is not necessary for
the assessment. Modules that are destined for their own external repos after the
theory is worked out are in the `other/` directory. The consuming modules 
specific to this app are in `/` and `iac`.

EB was an interesting choice because it is split in to three separate
concerns. An EB app is essentially a container of versions and environments and 
uses the *base* Terraform env. The EB environments needed to have their own
state for safe 'review app' provisioning but still needed to be able to reference
resources from the 'base' Terraform env. There are several points of interest 
with the dynamic remote state config and the decoupled EB App and EB Env plans.

A similar modularized and decoupled plan could use be used for any app that has
independent components with shared resources such as an MSA sharing a dedicated 
ECS cluster.

### continuous delivery

The implemented pipeline allows test jobs to fail which is the equivalent of 
continuous deployment rather than delivery. This is the primary motivator for
stopping the pipeline short of a production environment. No production, but one
of GitLab-CI's killer features are implemented - Review Apps. 

All of the EB Envs are provisioned in their own state with Terraform Environments
named after the git ref. Review apps can be dynamically provisioned and will be 
named as per the git branch as well. 

> Given that EB Envs (anything that starts new EC2s) take considerable time to 
provision, starting the Review App requires manual action but can still be 
auto-destroyed once a merge request is closed.

### prod env

The production environment could be another another CI Job that is triggered 
from git tags or from a release branch. The ebenv module used for the master env 
and for Review Apps can easily be used for a production environment as well.

BUT, EB rolling deployments can be quite slow and perhaps we would rather have 
use canary based deployments. AWS ALBs do not support weighted routing 
(yet) but we could accomplish something similar with weighted Route53 records, 
'green/blue' EB Envs, and GitLab CI jobs to updated the weighting of the R53 
aliases. Depending on how the team handled its continuous delivery, we might be 
able to ditch the long-lived stage env, master, for the two canary envs along
with review apps.
