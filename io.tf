variable "app_name" {
  type        = "string"
  description = "Name of the named resources"
}

variable "description" {
  default = ""

  description = <<EOF
(Optional)
Short description of the Application.
EOF
}

variable "app_zone" {
  type = "string"

  description = <<EOF
The app's base zone such that app_name.base_zone will be the FQDN.
Note: must already exsit as a delegated zone in R53
Note: app_name.app_zone will be used as the parent zone for EB Environments
EOF
}

variable "tags" {
  default = {}

  description = <<EOF
(Optional)
A set of tags to apply to the Application's taggable reasouces. 
EOF
}

output "app_zone_id" {
  value = "${module.eb.zone_id}"
}

output "app_zone_name" {
  value = "${module.eb.zone_name}"
}

output "app_name" {
  value = "${module.eb.name}"
}
