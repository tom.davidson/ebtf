variable "bucket" {}
variable "key" {}

variable "version_name" {
  default = ""
}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "base" {
  backend     = "s3"
  environment = "base"

  config {
    bucket = "${var.bucket}"
    key    = "${var.key}"
  }
}

module "ebenv" {
  source = "../../other/eb/environment"

  app_name      = "${data.terraform_remote_state.base.app_name}"
  app_zone_id   = "${data.terraform_remote_state.base.app_zone_id}"
  app_zone_name = "${data.terraform_remote_state.base.app_zone_name}"
  version_name  = "${var.version_name}"
}
