variable "app_name" {}

variable "tags" {
  default = {}
}

module "app_ops" {
  source = "../../other/app-ops"

  name = "${var.app_name}"
  tags = "${var.tags}"
}
