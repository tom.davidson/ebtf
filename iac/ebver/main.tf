variable "bucket" {}
variable "docker_image" {}
variable "key" {}
variable "version_name" {}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "base" {
  backend     = "s3"
  environment = "base"

  config {
    bucket = "${var.bucket}"
    key    = "${var.key}"
  }
}

module "ebver" {
  source = "../../other/eb/version"

  app_name     = "${data.terraform_remote_state.base.app_name}"
  bucket       = "${var.bucket}"
  docker_image = "${var.docker_image}"
  version_name = "${var.version_name}"
}
