module "eb" {
  source = "other/eb"

  app_name    = "${var.app_name}"
  description = "${var.description}"
  app_zone    = "${var.app_zone}"
  tags        = "${var.tags}"
}
